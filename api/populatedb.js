#!/usr/bin/env node

console.log(
  'This script populates some test apparels, footwear, headwear, ski and snowboards.'
);

// Get arguments passed on the command line
const userArgs = process.argv.slice(2);

const Apparel = require("./models/apparel");
const Footwear = require("./models/footwear");
const Headwear = require("./models/headwear");
const Ski = require("./models/ski");
const Snowboard = require("./models/snowboard");

const mongoose = require("mongoose");
mongoose.set("strictQuery", false);

const mongoDB = userArgs[0];

main().catch((err) => console.log(err));


async function main() {
  console.log("Debug: About to connect");
  await mongoose.connect(mongoDB);
  console.log("Debug: Should be connected?");
  await createApparel();
  await createFootwear();
  await createHeadwear();
  await createSnowboard();
  await createSki();
  console.log("Debug: Closing mongoose");
  mongoose.connection.close();
}

async function apparelCreate(index, brand, name, gender, size, color, year, type, price, sku, image) {
  const apparelItem = new Apparel({
    brand: brand,
    name: name,
    gender: gender,
    size: size,
    color: color,
    year: year,
    type: type,
    price: price,
    sku: sku,
    image: image
  });
  await apparelItem.save();
  console.log(`Added apparel: ${name}`);
}

async function footwearCreate(index, brand, name, gender, size, color, year, type, price, sku, image) {
  const footwearItem = new Footwear({
    brand: brand,
    name: name,
    gender: gender,
    size: size,
    color: color,
    year: year,
    type: type,
    price: price,
    sku: sku,
    image: image
  });
  await footwearItem.save();
  console.log(`Added footwear: ${name}`);
}

async function headwearCreate(index, brand, name, size, color, year, type, price, sku, image) {
  const headwearItem = new Headwear({
    brand: brand,
    name: name,
    size: size,
    color: color,
    year: year,
    type: type,
    price: price,
    sku: sku,
    image: image
  });
  await headwearItem.save();
  console.log(`Added headwear: ${name}`);
}

async function skiCreate(index, brand, name, gender, size, color, year, price, sku, image) {
  const skiItem = new Ski({
    brand: brand,
    name: name,
    gender: gender,
    size: size,
    color: color,
    year: year,
    price: price,
    sku: sku,
    image: image
  });
  await skiItem.save();
  console.log(`Added ski: ${name}`);
}

async function snowboardCreate(index, brand, name, gender, size, color, year, price, sku, image) {
  const snowboardItem = new Snowboard({
    brand: brand,
    name: name,
    gender: gender,
    size: size,
    color: color,
    year: year,
    price: price,
    sku: sku,
    image: image
  });
  await snowboardItem.save();
  console.log(`Added snowboard: ${name}`);
}

async function createApparel() {
  try {
    await Promise.all([
      apparelCreate(0, "Brand 1", "Apparel 1", "Men", "S", "Blue", 2023, "Jacket", 50.99, "SKU 1", "apparel1.jpg"),
      apparelCreate(1, "Brand 2", "Apparel 2", "Women", "M", "Black", 2023, "Pants", 55.99, "SKU 2", "apparel2.jpg"),
      // Add more apparel items here
    ]);
  } catch (err) {
    console.error("Error creating apparel:", err);
  }
}

async function createFootwear() {
  try {
    await Promise.all([
      footwearCreate(0, "Brand 1", "Footwear 1", "Men", "10", "Green", 2023, "Ski", 60.99, "SKU 3", "footwear1.jpg"),
      footwearCreate(1, "Brand 2", "Footwear 2", "Women", "9", "Yellow", 2023, "Snowboard", 65.99, "SKU 4", "footwear2.jpg"),
      // Add more footwear items here
    ]);
  } catch (err) {
    console.error("Error creating footwear:", err);
  }
}

async function createHeadwear() {
  try {
    await Promise.all([
      headwearCreate(0, "Brand 1", "Headwear 1", "S", "White", 2023, "Helmet", 30.99, "SKU 5", "headwear1.jpg"),
      headwearCreate(1, "Brand 2", "Headwear 2", "L", "Blue", 2023, "Hat", 35.99, "SKU 6", "headwear2.jpg"),
      // Add more headwear items here
    ]);
  } catch (err) {
    console.error("Error creating headwear:", err);
  }
}

async function createSki() {
  try {
    await Promise.all([
      skiCreate(0, "Brand 1", "Ski 1", "Men", "152", "White", 2023, 120.99, "SKU 7", "ski1.jpg"),
      skiCreate(1, "Brand 2", "Ski 2", "Women", "176", "Red", 2023, 130.99, "SKU 8", "ski2.jpg"),
      // Add more ski items here
    ]);
  } catch (err) {
    console.error("Error creating ski:", err);
  }
}

async function createSnowboard() {
  try {
    await Promise.all([
      snowboardCreate(0, "Brand 1", "Snowboard 1", "Men", "155", "White", 2023, 160.99, "SKU 9", "snowboard1.jpg"),
      snowboardCreate(1, "Brand 2", "Snowboard 2", "Unisex", "161", "Black", 2023, 170.99, "SKU 10", "snowboard2.jpg"),
      // Add more snowboard items here
    ]);
  } catch (err) {
    console.error("Error creating snowboard:", err);
  }
}
