const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const createError = require('http-errors');


const storeRouter = require('./routes/store');

var app = express();

// Set up mongoose connection
const mongoose = require("mongoose");
const config = require("./config");
const cors = require("cors");

mongoose.set("strictQuery", false);
const mongoDB = config.mongoURI;

main().catch((err) => console.log(err));
async function main() {
  await mongoose.connect(mongoDB);
}

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


// Configure CORS
var corsOptions = {
  origin: 'http://localhost:5173/',
};

app.use(cors());

app.use('/store', storeRouter);

// catch 404 and respond with JSON
app.use(function(req, res, next) {
  next(createError(404));
});

const logErrors = (err, req, res, next) => {
  console.error(err.stack); // Log the error stack trace
  next(err); // Pass the error to the next error-handling middleware
};

app.use(logErrors);

// error handler
app.use(function(err, req, res, next) {
  // respond with JSON for errors
  res.status(err.status || 500).json({ error: err.message });
});

app.listen(5000, () => { console.log("Server started on port 5000") });

module.exports = app;
