const express = require("express");
const router = express.Router();

// Require controller modules.
const apprarel_controller = require("../controllers/apparelController");
const footwear_controller = require("../controllers/footwearController");
const headwear_controller = require("../controllers/headwearController");
const ski_controller = require("../controllers/skiController");
const snowboard_controller = require("../controllers/snowboardController");


/// APPAREL ROUTES ///
// Get a list of apparels
router.get("/apparel", apprarel_controller.apparel_list);

// GET request - apparel detail
router.get("/apparel/:id", apprarel_controller.apparel_detail);

// POST request - create new apparel
router.post("/apparel/create", apprarel_controller.apparel_create_post);

// PUT request - update an apparel
router.put("/apparel/:id/update", apprarel_controller.apparel_update_put);

// DELETE request - delete an apparel
router.delete("/apparel/:id/delete", apprarel_controller.apparel_delete);


/// FOOTWEAR ROUTES ///
// Get a list of footwears
router.get("/footwear", footwear_controller.footwear_list);

// GET request - footwear detail
router.get("/footwear/:id", footwear_controller.footwear_detail);

// POST request - create new footwear
router.post("/footwear/create", footwear_controller.footwear_create_post);

// PUT request - update an footwear
router.put("/footwear/:id/update", footwear_controller.footwear_update_put);

// DELETE request - delete an footwear
router.delete("/footwear/:id/delete", footwear_controller.footwear_delete);


/// HEADWEAR ROUTES ///
// Get a list of headwears
router.get("/headwear", headwear_controller.headwear_list);

// GET request - headwear detail
router.get("/headwear/:id", headwear_controller.headwear_detail);

// POST request - create new headwear
router.post("/headwear/create", headwear_controller.headwear_create_post);

// PUT request - update an headwear
router.put("/headwear/:id/update", headwear_controller.headwear_update_put);

// DELETE request - delete an headwear
router.delete("/headwear/:id/delete", headwear_controller.headwear_delete);


// SKI ROUTES ///
// Get a list of skis
router.get("/ski", ski_controller.ski_list);

// GET request - ski detail
router.get("/ski/:id", ski_controller.ski_detail);

// POST request - create new ski
router.post("/ski/create", ski_controller.ski_create_post);

// PUT request - update an ski
router.put("/ski/:id/update", ski_controller.ski_update_put);

// DELETE request - delete an ski
router.delete("/ski/:id/delete", ski_controller.ski_delete);


// SNOWBOARD ROUTES ///
// Get a list of snowboards
router.get("/snowboard", snowboard_controller.snowboard_list);

// GET request - snowboard detail
router.get("/snowboard/:id", snowboard_controller.snowboard_detail);

// POST request - create new snowboard
router.post("/snowboard/create", snowboard_controller.snowboard_create_post);

// PUT request - update an snowboard
router.put("/snowboard/:id/update", snowboard_controller.snowboard_update_put);

// DELETE request - delete an snowboard
router.delete("/snowboard/:id/delete", snowboard_controller.snowboard_delete);

module.exports = router;
