const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const HeadwearSchema = new Schema({
    brand: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    size: {
        type: String,
        enum: ['S', 'M', 'L', 'XL']
    },
    color: String,
    year: Number,
    type: {
        type: String,
        enum: ['Helmet', 'Hat']
    },
    price: {
        type: Number,
        required: true
    },
    sku: {
        type: String,
        unique: true
    },
    image: {
        type: String,
    }
});


// Virtual for headwear's URL
HeadwearSchema.virtual("url").get(function () {
    // We don't use an arrow function as we'll need the this object
    return `/api/headwear/${this._id}`;
  });

// Create a model using the schema
const Headwear = mongoose.model('Headwear', HeadwearSchema);

module.exports = Headwear;
