const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const SnowboardSchema = new Schema({
    brand: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: ['Men', 'Women', 'Unisex']
    },
    size: Number,
    color: String,
    year: Number,
    price: {
        type: Number,
        required: true
    },
    sku: {
        type: String,
        unique: true
    },
    image: {
        type: String,
    }
});

// Virtual for snowboards's URL
SnowboardSchema.virtual("url").get(function () {
    // We don't use an arrow function as we'll need the this object
    return `/api/snowboard/${this._id}`;
  });

// Create a model using the schema
const Snowboard = mongoose.model('Snowboard', SnowboardSchema);

module.exports = Snowboard;
