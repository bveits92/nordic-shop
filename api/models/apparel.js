const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ApparelSchema = new Schema({
    brand: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: ['Men', 'Women', 'Unisex']
    },
    size: {
        type: String,
        enum: ['S', 'M', 'L', 'XL']
    },
    color: String,
    year: Number,
    type: {
        type: String,
        enum: ['Jacket', 'Pants', 'Gloves', 'Bib']
    },
    price: {
        type: Number,
        required: true
    },
    sku: {
        type: String,
        unique: true
    },
    image: {
        type: String,
    }
});


// Virtual for apparel's URL
ApparelSchema.virtual("url").get(function () {
    // We don't use an arrow function as we'll need the this object
    return `store/apparel/${this._id}`;
  });

// Create a model using the schema
const Apparel = mongoose.model('Apparel', ApparelSchema);

module.exports = Apparel;
