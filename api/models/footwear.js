const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const FootwearSchema = new Schema({
    brand: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: ['Men', 'Women', 'Unisex']
    },
    size: Number,
    color: String,
    year: Number,
    type: {
        type: String,
        enum: ['Ski', 'Snowboard']
    },
    price: {
        type: Number,
        required: true
    },
    sku: {
        type: String,
        unique: true
    },
    image: {
        type: String,
    }
});

// Virtual for footwear's URL
FootwearSchema.virtual("url").get(function () {
    // We don't use an arrow function as we'll need the this object
    return `/api/footwear/${this._id}`;
  });

// Create a model using the schema
const Footwear = mongoose.model('Footwear', FootwearSchema);

module.exports = Footwear;
