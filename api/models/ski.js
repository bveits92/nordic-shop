const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const SkiSchema = new Schema({
    brand: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: ['Men', 'Women', 'Unisex']
    },
    size: Number,
    color: String,
    year: Number,
    price: {
        type: Number,
        required: true
    },
    sku: {
        type: String,
        unique: true
    },
    image: {
        type: String,
    }
});


// Virtual for ski's URL
SkiSchema.virtual("url").get(function () {
    // We don't use an arrow function as we'll need the this object
    return `/api/ski/${this._id}`;
  });

// Create a model using the schema
const Ski = mongoose.model('Ski', SkiSchema);

module.exports = Ski;
