const Footwear = require("../models/footwear");
const asyncHandler = require("express-async-handler");
const { body, validationResult } = require("express-validator");

// Display list of all footwears
exports.footwear_list = asyncHandler(async (req, res, next) => {
    const allFootwear = await Footwear.find().sort({ name: 1 }).exec();
    res.json(allFootwear);
});

// Display an footwear detail
exports.footwear_detail = asyncHandler(async (req, res) => {
    try {
        const footwear = await Footwear.findById(req.params.id).exec();

        if (!footwear) { // It's better to check with '!'
            // No results
            const err = new Error("Footwear not found");
            err.status = 404;
            throw err;
        }

        // Send the footwear object as JSON
        res.json(footwear);
    } catch (err) {
        // Handle errors and send an error JSON response
        res.status(err.status || 500).json({ error: err.message });
    }
});

// Handle footwear create on POST
exports.footwear_create_post = [
    // Validate and sanitize fields.
    body("brand", "Brand must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("name", "Name must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("gender", "Gender must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("size", "Size must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("color", "Color must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("year", "Year must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("type", "Type must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("price", "Price must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("sku", "SKU must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("image", "Image must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),

    async (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create an footwear object with escaped and trimmed data.
        const footwear = new Footwear({
            brand: req.body.brand,
            name: req.body.name,
            gender: req.body.gender,
            size: req.body.size,
            color: req.body.color,
            year: req.body.year,
            type: req.body.type,
            price: req.body.price,
            sku: req.body.sku,
            image: req.body.image,
        });

        if (!errors.isEmpty()) {
            // There are errors. Return a JSON response with error messages.
            res.status(400).json({ errors: errors.array() });
        } else {
            // Data from JSON request is valid. Save footwear.
            await footwear.save();
            res.status(201).json(footwear); // Return JSON response with the created footwear object.
        }
    },
];

// Handle footwear update on PUT
exports.footwear_update_put = [
    // Validate and sanitize fields.
    body("brand", "Brand must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("name", "Name must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("gender", "Gender must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("size", "Size must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("color", "Color must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("year", "Year must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("type", "Type must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("price", "Price must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("sku", "SKU must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("image", "Image must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),

    async (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            // There are errors. Return a JSON response with error messages.
            res.status(400).json({ errors: errors.array() });
        } else {
            try {
                // Find the existing footwear item by ID (assuming you have a valid ID parameter).
                const existingFootwear = await Footwear.findById(req.params.id);

                if (!existingFootwear) {
                    // If the footwear item does not exist, return a 404 error.
                    res.status(404).json({ error: "Footwear item not found" });
                } else {
                    // Update the existing footwear item with the new data.
                    existingFootwear.brand = req.body.brand;
                    existingFootwear.name = req.body.name;
                    existingFootwear.gender = req.body.gender;
                    existingFootwear.size = req.body.size;
                    existingFootwear.color = req.body.color;
                    existingFootwear.year = req.body.year;
                    existingFootwear.type = req.body.type;
                    existingFootwear.price = req.body.price;
                    existingFootwear.sku = req.body.sku;
                    existingFootwear.image = req.body.image;

                    // Save the updated footwear item.
                    await existingFootwear.save();
                    res.status(200).json(existingFootwear); // Return JSON response with the updated footwear object.
                }
            } catch (err) {
                // Handle any errors that occur during the update process.
                res.status(500).json({ error: "Internal Server Error" });
            }
        }
    },
];

// Handle footwear delete on DELETE
exports.footwear_delete = asyncHandler(async (req, res) => {
    try {
        // Find the footwear item by ID and delete it.
        const footwear = await Footwear.findByIdAndDelete(req.params.id);

        if (!footwear) {
            // If the footwear item does not exist, return a 404 error.
            return res.status(404).json({ error: "Footwear item not found" });
        } else {
            // The footwear item was deleted.
            return res.status(200).json({ message: "Footwear item deleted" });
        }
    } catch (err) {
        // Log the error for debugging purposes.
        console.error(err);

        // Handle any errors that occur during the delete process.
        return res.status(500).json({ error: err.message });
    }
});
