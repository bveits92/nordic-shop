const Snowboard = require("../models/snowboard"); // Replace with your actual snowboard model
const asyncHandler = require("express-async-handler");
const { body, validationResult } = require("express-validator");

// Display list of all snowboards
exports.snowboard_list = asyncHandler(async (req, res, next) => {
    const allSnowboards = await Snowboard.find().sort({ name: 1 }).exec();
    res.json(allSnowboards);
});

// Display a snowboard detail
exports.snowboard_detail = asyncHandler(async (req, res) => {
    try {
        const snowboard = await Snowboard.findById(req.params.id).exec();

        if (!snowboard) {
            const err = new Error("Snowboard not found");
            err.status = 404;
            throw err;
        }

        res.json(snowboard);
    } catch (err) {
        res.status(err.status || 500).json({ error: err.message });
    }
});

// Handle snowboard create on POST
exports.snowboard_create_post = [
    body("brand", "Brand must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("name", "Name must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("gender", "Gender must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("size", "Size must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("color", "Color must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("year", "Year must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("price", "Price must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("sku", "SKU must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("image", "Image must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),

    async (req, res, next) => {
        const errors = validationResult(req);

        const snowboard = new Snowboard({
            brand: req.body.brand,
            name: req.body.name,
            gender: req.body.gender,
            size: req.body.size,
            color: req.body.color,
            year: req.body.year,
            price: req.body.price,
            sku: req.body.sku,
            image: req.body.image,
        });

        if (!errors.isEmpty()) {
            res.status(400).json({ errors: errors.array() });
        } else {
            await snowboard.save();
            res.status(201).json(snowboard);
        }
    },
];

// Handle snowboard update on PUT
exports.snowboard_update_put = [
    body("brand", "Brand must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("name", "Name must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("gender", "Gender must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("size", "Size must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("color", "Color must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("year", "Year must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("price", "Price must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("sku", "SKU must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("image", "Image must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),

    async (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            res.status(400).json({ errors: errors.array() });
        } else {
            try {
                const existingSnowboard = await Snowboard.findById(req.params.id);

                if (!existingSnowboard) {
                    res.status(404).json({ error: "Snowboard item not found" });
                } else {
                    existingSnowboard.brand = req.body.brand;
                    existingSnowboard.name = req.body.name;
                    existingSnowboard.gender = req.body.gender,
                    existingSnowboard.size = req.body.size;
                    existingSnowboard.color = req.body.color;
                    existingSnowboard.year = req.body.year;
                    existingSnowboard.price = req.body.price;
                    existingSnowboard.sku = req.body.sku;
                    existingSnowboard.image = req.body.image;

                    await existingSnowboard.save();
                    res.status(200).json(existingSnowboard);
                }
            } catch (err) {
                console.error(err);
                res.status(500).json({ error: "Internal Server Error" });
            }
        }
    },
];

// Handle snowboard delete on DELETE
exports.snowboard_delete = asyncHandler(async (req, res) => {
    try {
        const snowboard = await Snowboard.findByIdAndDelete(req.params.id);

        if (!snowboard) {
            return res.status(404).json({ error: "Snowboard item not found" });
        } else {
            return res.status(200).json({ message: "Snowboard item deleted" });
        }
    } catch (err) {
        console.error(err);
        return res.status(500).json({ error: err.message });
    }
});
