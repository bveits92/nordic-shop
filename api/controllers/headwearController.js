const Headwear = require("../models/headwear");
const asyncHandler = require("express-async-handler");
const { body, validationResult } = require("express-validator");

// Display list of all headwear
exports.headwear_list = asyncHandler(async (req, res, next) => {
    const allHeadwear = await Headwear.find().sort({ name: 1 }).exec();
    res.json(allHeadwear);
});

// Display a headwear detail
exports.headwear_detail = asyncHandler(async (req, res) => {
    try {
        const headwear = await Headwear.findById(req.params.id).exec();

        if (!headwear) {
            const err = new Error("Headwear not found");
            err.status = 404;
            throw err;
        }

        res.json(headwear);
    } catch (err) {
        res.status(err.status || 500).json({ error: err.message });
    }
});

// Handle headwear create on POST
exports.headwear_create_post = [
    body("brand", "Brand must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("name", "Name must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("size", "Size must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("color", "Color must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("year", "Year must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("type", "Type must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("price", "Price must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("sku", "SKU must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("image", "Image must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),

    async (req, res, next) => {
        const errors = validationResult(req);

        const headwear = new Headwear({
            brand: req.body.brand,
            name: req.body.name,
            size: req.body.size,
            color: req.body.color,
            year: req.body.year,
            type: req.body.type,
            price: req.body.price,
            sku: req.body.sku,
            image: req.body.image,
        });

        if (!errors.isEmpty()) {
            res.status(400).json({ errors: errors.array() });
        } else {
            await headwear.save();
            res.status(201).json(headwear);
        }
    },
];

// Handle headwear update on PUT
exports.headwear_update_put = [
    body("brand", "Brand must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("name", "Name must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("size", "Size must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("color", "Color must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("year", "Year must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("type", "Type must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("price", "Price must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("sku", "SKU must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("image", "Image must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),

    async (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            res.status(400).json({ errors: errors.array() });
        } else {
            try {
                const existingHeadwear = await Headwear.findById(req.params.id);

                if (!existingHeadwear) {
                    res.status(404).json({ error: "Headwear item not found" });
                } else {
                    existingHeadwear.brand = req.body.brand;
                    existingHeadwear.name = req.body.name;
                    existingHeadwear.size = req.body.size;
                    existingHeadwear.color = req.body.color;
                    existingHeadwear.year = req.body.year;
                    existingHeadwear.type = req.body.type;
                    existingHeadwear.price = req.body.price;
                    existingHeadwear.sku = req.body.sku;
                    existingHeadwear.image = req.body.image;

                    await existingHeadwear.save();
                    res.status(200).json(existingHeadwear);
                }
            } catch (err) {
                console.error(err);
                res.status(500).json({ error: "Internal Server Error" });
            }
        }
    },
];

// Handle headwear delete on DELETE
exports.headwear_delete = asyncHandler(async (req, res) => {
    try {
        const headwear = await Headwear.findByIdAndDelete(req.params.id);

        if (!headwear) {
            return res.status(404).json({ error: "Headwear item not found" });
        } else {
            return res.status(200).json({ message: "Headwear item deleted" });
        }
    } catch (err) {
        console.error(err);
        return res.status(500).json({ error: err.message });
    }
});
