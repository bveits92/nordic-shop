const Ski = require("../models/ski");
const asyncHandler = require("express-async-handler");
const { body, validationResult } = require("express-validator");

// Display list of all skis
exports.ski_list = asyncHandler(async (req, res, next) => {
    const allSkis = await Ski.find().sort({ name: 1 }).exec();
    res.json(allSkis);
});

// Display a ski detail
exports.ski_detail = asyncHandler(async (req, res) => {
    try {
        const ski = await Ski.findById(req.params.id).exec();

        if (!ski) {
            const err = new Error("Ski not found");
            err.status = 404;
            throw err;
        }

        res.json(ski);
    } catch (err) {
        res.status(err.status || 500).json({ error: err.message });
    }
});

// Handle ski create on POST
exports.ski_create_post = [
    body("brand", "Brand must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("name", "Name must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("gender", "Gender must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("size", "Size must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("color", "Color must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("year", "Year must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("price", "Price must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("sku", "SKU must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("image", "Image must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),

    async (req, res, next) => {
        const errors = validationResult(req);

        const ski = new Ski({
            brand: req.body.brand,
            name: req.body.name,
            gender: req.body.gender,
            size: req.body.size,
            color: req.body.color,
            year: req.body.year,
            price: req.body.price,
            sku: req.body.sku,
            image: req.body.image,
        });

        if (!errors.isEmpty()) {
            res.status(400).json({ errors: errors.array() });
        } else {
            await ski.save();
            res.status(201).json(ski);
        }
    },
];

// Handle ski update on PUT
exports.ski_update_put = [
    body("brand", "Brand must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("name", "Name must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("gender", "Gender must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("size", "Size must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("color", "Color must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("year", "Year must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("price", "Price must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("sku", "SKU must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("image", "Image must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),

    async (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            res.status(400).json({ errors: errors.array() });
        } else {
            try {
                const existingSki = await Ski.findById(req.params.id);

                if (!existingSki) {
                    res.status(404).json({ error: "Ski item not found" });
                } else {
                    existingSki.brand = req.body.brand;
                    existingSki.name = req.body.name;
                    existingSki.gender = req.body.gender,
                    existingSki.size = req.body.size;
                    existingSki.color = req.body.color;
                    existingSki.year = req.body.year;
                    existingSki.price = req.body.price;
                    existingSki.sku = req.body.sku;
                    existingSki.image = req.body.image;

                    await existingSki.save();
                    res.status(200).json(existingSki);
                }
            } catch (err) {
                console.error(err);
                res.status(500).json({ error: "Internal Server Error" });
            }
        }
    },
];

// Handle ski delete on DELETE
exports.ski_delete = asyncHandler(async (req, res) => {
    try {
        const ski = await Ski.findByIdAndDelete(req.params.id);

        if (!ski) {
            return res.status(404).json({ error: "Ski item not found" });
        } else {
            return res.status(200).json({ message: "Ski item deleted" });
        }
    } catch (err) {
        console.error(err);
        return res.status(500).json({ error: err.message });
    }
});
