const Apparel = require("../models/apparel");
const asyncHandler = require("express-async-handler");
const { body, validationResult } = require("express-validator");
const multer = require('multer');
const bodyParser = require('body-parser');

// Display list of all apparels
exports.apparel_list = asyncHandler(async (req, res, next) => {
    const allApparels = await Apparel.find().sort({ name: 1 }).exec();
    res.json(allApparels);
});

// Display an apparel detail
exports.apparel_detail = asyncHandler(async (req, res) => {
    try {
        const apparel = await Apparel.findById(req.params.id).exec();

        if (!apparel) { // It's better to check with '!'
            // No results
            const err = new Error("Apparel not found");
            err.status = 404;
            throw err;
        }

        // Send the apparel object as JSON
        res.json(apparel);
    } catch (err) {
        // Handle errors and send an error JSON response
        res.status(err.status || 500).json({ error: err.message });
    }
});

// Define storage for uploaded images
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/'); // Specify the directory where you want to store the images
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
        cb(null, uniqueSuffix + '-' + file.originalname);
    }
});

const upload = multer({ storage: storage });

// Handle apparel create on POST
exports.apparel_create_post = [
    upload.single('image'),
    // Validate and sanitize fields.
    body("brand", "Brand must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("name", "Name must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("gender", "Gender must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("size", "Size must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("color", "Color must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("year", "Year must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("type", "Type must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("price", "Price must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("sku", "SKU must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),


    asyncHandler(async (req, res, next) => {
        // Check if an image was uploaded
        if (!req.file) {
            return res.status(400).json({ errors: [{ msg: 'Image must be provided.' }] });
        }

        // The image file is stored in req.file.path, you can save this path in your MongoDB document
        const imagePath = req.file.path;

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create an Apparel object with escaped and trimmed data.
        const apparel = new Apparel({
            brand: req.body.brand,
            name: req.body.name,
            gender: req.body.gender,
            size: req.body.size,
            color: req.body.color,
            year: req.body.year,
            type: req.body.type,
            price: req.body.price,
            sku: req.body.sku,
            image: imagePath, // Store the image path in your document
        });

        if (!errors.isEmpty()) {
            // There are errors. Return a JSON response with error messages.
            return res.status(400).json({ errors: errors.array() });
        } else {
            // Data from JSON request is valid. Save apparel.
            await apparel.save();
            return res.status(201).json(apparel); // Return JSON response with the created apparel object.
        }
    }),
];

// Handle apparel update on PUT
exports.apparel_update_put = [
    // Validate and sanitize fields.
    body("brand", "Brand must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("name", "Name must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("gender", "Gender must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("size", "Size must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("color", "Color must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("year", "Year must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("type", "Type must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("price", "Price must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("sku", "SKU must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),
    body("image", "Image must not be empty.")
        .trim()
        .isLength({ min: 1 })
        .escape(),

    async (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            // There are errors. Return a JSON response with error messages.
            res.status(400).json({ errors: errors.array() });
        } else {
            try {
                // Find the existing apparel item by ID (assuming you have a valid ID parameter).
                const existingApparel = await Apparel.findById(req.params.id);

                if (!existingApparel) {
                    // If the apparel item does not exist, return a 404 error.
                    res.status(404).json({ error: "Apparel item not found" });
                } else {
                    // Update the existing apparel item with the new data.
                    existingApparel.brand = req.body.brand;
                    existingApparel.name = req.body.name;
                    existingApparel.gender = req.body.gender;
                    existingApparel.size = req.body.size;
                    existingApparel.color = req.body.color;
                    existingApparel.year = req.body.year;
                    existingApparel.type = req.body.type;
                    existingApparel.price = req.body.price;
                    existingApparel.sku = req.body.sku;
                    existingApparel.image = req.body.image;

                    // Save the updated apparel item.
                    await existingApparel.save();
                    res.status(200).json(existingApparel); // Return JSON response with the updated apparel object.
                }
            } catch (err) {
                // Handle any errors that occur during the update process.
                res.status(500).json({ error: "Internal Server Error" });
            }
        }
    },
];

// Handle apparel delete on DELETE
exports.apparel_delete = asyncHandler(async (req, res) => {
    try {
        // Find the apparel item by ID and delete it.
        const apparel = await Apparel.findByIdAndDelete(req.params.id);

        if (!apparel) {
            // If the apparel item does not exist, return a 404 error.
            return res.status(404).json({ error: "Apparel item not found" });
        } else {
            // The apparel item was deleted.
            return res.status(200).json({ message: "Apparel item deleted" });
        }
    } catch (err) {
        // Log the error for debugging purposes.
        console.error(err);

        // Handle any errors that occur during the delete process.
        return res.status(500).json({ error: err.message });
    }
});
