import Button from "@mui/material/Button";
import { CreateDiv } from "../../src/styles/styles";
import { useNavigate } from "react-router-dom";

function Apparel() {
    const Navigate = useNavigate();
    
    function createApparel() {
        Navigate("/apparel/create");
    }

    return (
        <CreateDiv>
            <div>
                <Button onClick={() => createApparel()} variant="contained" color="success">
                    Create Apparel
                </Button>
            </div>
        </CreateDiv>
    );
}

export default Apparel;
