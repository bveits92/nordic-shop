import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { H2 } from "../../src/styles/styles";
import { MenuItem, InputAdornment } from "@mui/material";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Input from "@mui/material/Input";


const CreateApparel = () => {
    const [brand, setBrand] = useState("");
    const [name, setName] = useState("");
    const [gender, setGender] = useState("");
    const [size, setSize] = useState("");
    const [color, setColor] = useState("");
    const [year, setYear] = useState("");
    const [type, setType] = useState("");
    const [price, setPrice] = useState("");
    const [sku, setSku] = useState("");
    const [image, setImage] = useState(null);

    const navigate = useNavigate();
    const [showDialog, setShowDialog] = useState(false);
    const [error, setError] = useState(null);
    const baseUrl = import.meta.env.VITE_REACT_APP_API_HOST;

    const showSuccessDialog = () => {
        setShowDialog(true);
        setTimeout(() => {
            setShowDialog(false);
        }, 3000);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const formData = new FormData();
        formData.append("brand", brand);
        formData.append("name", name);
        formData.append("gender", gender);
        formData.append("size", size);
        formData.append("color", color);
        formData.append("year", year);
        formData.append("type", type);
        formData.append("price", price);
        formData.append("sku", sku);
        formData.append("image", image);

        const createUrl = `${baseUrl}/store/apparel/create/`;
        console.log(formData);
        try {
            const response = await fetch(createUrl, {
                method: "POST",
                body: formData,
            });
            const json = await response.json();
            if (json.error) {
                setError(json.error);
            } else {
                showSuccessDialog();
                navigate("/apparel");
            }
        } catch (error) {
            setError(error);
        }
    };

    const sizeOptions = ['S', 'M', 'L', 'XL'];
    const genderOptions = ['Men', 'Women', 'Unisex'];
    const typeOptions = ['Jacket', 'Pants', 'Gloves', 'Bib'];

    return (
        <Container style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
            <Grid container spacing={8}
                direction="row"
                justifyContent="center"
                alignItems="center"
                sx={{ height: '100vh' }}
            >
                <Grid item xs={8}>
                    <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                            p: 6,
                            border: "1px solid grey",
                            backgroundColor: "rgba(255, 255, 255, 0.3)",
                            boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
                        }}
                        noValidate
                        autoComplete='off'
                    >
                        <H2>Create Apparel</H2>

                        <TextField
                            label="Brand"
                            id="brand"
                            value={brand}
                            onChange={(e) => setBrand(e.target.value)}
                            placeholder="Brand"
                            sx={{ mb: 1 }}
                        />
                        <TextField
                            label="Name"
                            id="name"
                            autoComplete="off"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            placeholder="Name"
                            sx={{ mb: 1 }}
                        />

                        <TextField
                            id="gender"
                            name="gender"
                            label="Gender"
                            select
                            value={gender}
                            onChange={(e) => setGender(e.target.value)}
                            placeholder="Gender"
                            sx={{ mb: 1, width: '37%' }} // Add spacing if needed
                        >
                            {genderOptions.map((option) => (
                                <MenuItem key={option} value={option}>
                                    {option}
                                </MenuItem>
                            ))}
                        </TextField>

                        <TextField
                            label="Size"
                            id="size"
                            name="size"
                            select
                            value={size}
                            onChange={(e) => setSize(e.target.value)}
                            placeholder="Size"
                            sx={{ mb: 1, width: '37%' }} // Add spacing if needed
                        >
                            {sizeOptions.map((option) => (
                                <MenuItem key={option} value={option}>
                                    {option}
                                </MenuItem>
                            ))}
                        </TextField>

                        <TextField
                            label="Color"
                            id="color"
                            value={color}
                            onChange={(e) => setColor(e.target.value)}
                            placeholder="Color"
                            sx={{ mb: 1 }}
                        />
                        <TextField
                            label="Year"
                            id="year"
                            value={year}
                            type="number"
                            onChange={(e) => setYear(e.target.value)}
                            placeholder="Year"
                            sx={{ mb: 1 }}
                        />

                        <TextField
                            label="Type"
                            id="type"
                            name="type"
                            select
                            value={type}
                            onChange={(e) => setType(e.target.value)}
                            placeholder="Type"
                            sx={{ mb: 1, width: '37%' }}
                        >
                            {typeOptions.map((option) => (
                                <MenuItem key={option} value={option}>
                                    {option}
                                </MenuItem>
                            ))}
                        </TextField>

                        <TextField
                            label="Price"
                            id="price"
                            value={price}
                            type="number"
                            onChange={(e) => setPrice(e.target.value)}
                            placeholder="Price"
                            InputProps={{
                                startAdornment: <InputAdornment position="start">$</InputAdornment>,
                            }}
                            sx={{ mb: 1, width: '37%' }}
                        />

                        <TextField
                            label="SKU"
                            id="sku"
                            value={sku}
                            type="number"
                            onChange={(e) => setSku(e.target.value)}
                            placeholder="SKU"
                            sx={{ mb: 1 }}
                        />

                        <Button
                            variant="contained"
                            component="label"
                            sx={{ mb: 1 }}
                        >
                            Upload File
                            <input
                            type="file"
                            hidden
                            onChange={(e) => setImage(e.target.files[0])}
                            />
                        </Button>

                        <Button
                            style={{ backgroundColor: 'brightblue', color: 'white', width: '28ch' }}
                            variant='contained'
                            type='submit'
                            onClick={handleSubmit}
                            >
                            Create Apparel
                        </Button>
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
}

export default CreateApparel;
