import React from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";

function HomePage() {
    return (
        <Container style={{ display: "flex", marginTop: 20, marginLeft: 10 }}>
            <Grid container spacing={3}>
                <Grid item xs={10}>
                    <Box
                        sx={{
                            p: 6,
                            border: "1px solid grey",
                            backgroundColor: "rgba(255, 255, 255, 0.3)",
                            boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
                        }}
                    >
                        <Typography
                            variant="h2"
                            gutterBottom
                            style={{
                                color: "white",
                                textShadow: "2px 2px 4px rgba(0, 0, 0, 0.5)",
                                textOutline: "2px solid white",
                                WebkitTextStroke: "2px white",
                                WebkitTextFillColor: "rgba(0, 0, 0, 0.5)",
                                filter: "drop-shadow(2px 2px 4px rgba(0, 0, 0, 0.5))",
                                WebkitFilter: "drop-shadow(2px 2px 4px rgba(0, 0, 0, 0.5))",

                            }}
                        >
                            Welcome to Nordic Ski Shop
                        </Typography>
                        <Typography variant="body1" paragraph>
                            Explore a world of skiing excellence with Nordic Ski Shop. We are
                            your premier destination for top-quality ski equipment, apparel,
                            and accessories.
                        </Typography>
                        <Typography variant="body1" paragraph>
                            Whether you're a seasoned pro or just starting your skiing
                            journey, we have everything you need to conquer the slopes. From
                            high-performance skis and bindings to stylish winter wear, we've
                            got you covered.
                        </Typography>
                        <Typography variant="body1" paragraph>
                            Join us in the adventure of a lifetime. Gear up, hit the
                            mountains, and experience the thrill of skiing like never before.
                        </Typography>
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
}

export default HomePage;
