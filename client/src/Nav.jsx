import React from 'react';
import { StyledNavbar } from './styles/styles';
import { Typography } from '@mui/material';
import { Toolbar } from '@mui/material';
import { Link } from 'react-router-dom';
import { StyledButton } from './styles/styles';

export default function Nav() {
  return (
    <StyledNavbar position='static'>
        <Toolbar disableGutters sx={{
            justifyContent: 'flex-start',
            marginLeft: '10px',
            }}>
          <Typography
            variant='h6'
            noWrap
            component={Link}
            to='/'
            sx={{
              mr: 5,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none'
            }}
          >
            Nordic-Shop
          </Typography>
          <StyledButton variant="contained" component={Link} to='/apparel'>
            Apparel
          </StyledButton>
          <StyledButton variant="contained" component={Link} to='/footwear'>
            Footwear
          </StyledButton>
          <StyledButton variant="contained" component={Link} to='/headwear'>
            Headwear
          </StyledButton>
          <StyledButton variant="contained" component={Link} to='/ski'>
            Ski
          </StyledButton>
          <StyledButton variant="contained" component={Link} to='/snowboard'>
            Snowboard
          </StyledButton>
        </Toolbar>
    </StyledNavbar>
  );
}
