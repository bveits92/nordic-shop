import styled from '@emotion/styled';
import AppBar from '@mui/material/AppBar';
import { Button } from '@mui/material';
import { Global, css } from '@emotion/react';
import backgroundImage from '../assets/background.png';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';



export const GlobalStyles = () => (
    <Global
      styles={css`
        body {
          margin: 0;
          padding: 0;
          background-image: url(${backgroundImage});
          background-size: cover;
          background-position: center;
          background-repeat: no-repeat;
          height: 100vh;
        }
      `}
    />
  );

export const StyledNavbar = styled(AppBar)`
    background-color: #545554 !important;
    margin: 0;
    width: 100vw;
`;


export const StyledButton = styled(Button)`
    color: white;
    background-color: #545554;
    box-shadow: none !important;
    border-radius: 0 !important;
    && {
        :hover {
            background-color: #ea7406 !important;
            box-shadow: none !important;
        }
    }
`;


export const CreateDiv = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 5vh;
`;


export const GridItems = styled(Grid)`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 5vh;
`;

export const H2 = styled.h2`
    color: white;
    text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);
    font-weight: bold;
    font-size: 24px;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const FormBox = styled(Box)`
    p: 6,
    border: "1px solid grey",
    backgroundColor: "rgba(255, 255, 255, 0.3)",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
`;


