import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "../pages/HomePage";
import Nav from "./Nav";
import { GlobalStyles } from "./styles/styles";
import Apparel from "../pages/Apparel/Apparel";
import CreateApparel from "../pages/Apparel/CreateApparel";

const apiUrl = import.meta.env.VITE_REACT_APP_API_HOST;
if (!apiUrl) {
  throw new Error("VITE_REACT_APP_API_HOST was undefined.");
}

function App() {
 return (
    <>
    <GlobalStyles />
      <BrowserRouter baseUrl={apiUrl}>
        <Nav />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/apparel" element={<Apparel />} />
          <Route path="/apparel/create" element={<CreateApparel />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
